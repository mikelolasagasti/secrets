# Secrets analyzer changelog

## v5.2.5
- Update `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!296)
- Set the Go version to `v1.21` to keep it consistent with docker images (!296)
- Update Docker FIPS image [`v1.21.5` => `v1.21.9`] (!296)

## v5.2.4
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!288)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!288)
- upgrade Go version to `v1.21` and `v1.21.5` for non-FIPS and FIPS images, respectively (!288)

## v5.2.3
- Check out all commits for merge request branches to prevent inaccurate commit ranges (!285 @morgan_delagrange)
- Prevent scan failures when git history is rewritten (!285 @morgan_delagrange)

## v5.2.2
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.18.1` => [`8.18.2`](https://github.com/gitleaks/gitleaks/releases/tag/v8.18.2)] (!281)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!281)

## v5.2.1
- Revert changes from v5.2.0 as they were causing errors under some scenarios where git history was being rewritten (!283)

## v5.2.0
- Check out all commits for merge request branches to prevent inaccurate commit ranges (!252 @morgan_delagrange)

## v5.1.19
- Add detection rules for SCIM tokens and CI Build (Job) tokens (!279)

## v5.1.18
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!277)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!277)

## v5.1.17
- Add word boundaries to Ionic token to reduce false positives (!273)

## v5.1.16
- Update gitlab_pipeline_trigger_token to be 40 bytes instead of 20 since it's a hex string (!269)
- Update Slack Token to not use `?` qualifier as it could incorrectly match the slack token prefix only.
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.18.0` => [`8.18.1`](https://github.com/gitleaks/gitleaks/releases/tag/v8.18.1)] (!268)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!268)

## v5.1.15
- Add detection rule for GitLab Deploy Tokens (!270)

## v5.1.14
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!265)
  - Update Secure Report Schema version to 15.0.7

## v5.1.13
- Add word boundaries to GitLab and AWS tokens to reduce false positives (!256)

## v5.1.12
- Add detection rule for Tailscale tokens (!247 @samlinville-ts)

## v5.1.11
- Add detection rule for GitLab OAuth application secret, GitLab feed token, GitLab incoming email token and GitLab Agent for Kubernetes token (!254)

## v5.1.10
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!253)

## v5.1.9
- upgrade [`common`](gitlab.com/gitlab-org/security-products/analyzers/common/v3) to [`3.2.3`]((https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)) (!246)
  - Fix trusting Custom CA Certificate for UBI-based images
- Move custom CA bundle file path to trust anchors location in FIPS docker image (!246)

## v5.1.8
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.17.0` => [`8.18.0`](https://github.com/gitleaks/gitleaks/releases/tag/v8.18.0)] (!244)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.5` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!244)

## v5.1.7
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.5`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.5)] (!241)
  - Fix incorrect reference to Remote Custom ruleset file

## v5.1.6
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!237)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!237)

## v5.1.5
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.16.3` => [`8.17.0`](https://github.com/gitleaks/gitleaks/releases/tag/v8.17.0)] (!229)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.5` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!229)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!229)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!229)
- Drop  deprecated `secret.Message` field when building report (!229)

## v5.1.4
- Add detection rule for CircleCI PRJ and PAT tokens (!226 @nathanwfish)

## v5.1.3
- Add detection rule for Open AI API key (!227)

## v5.1.2
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (gitlab-org/security-products/analyzers/secrets!222)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (gitlab-org/security-products/analyzers/secrets!222)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (gitlab-org/security-products/analyzers/secrets!222)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (gitlab-org/security-products/analyzers/secrets!222)

## v5.1.1
- Ensure keywords are case-sensitive (!220)

## v5.1.0
- Update `ruleset` module to `v2.0.2` to support loading remote Custom Rulesets (!217)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (!217)

## v5.0.1
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.16.2` => [`8.16.3`](https://github.com/gitleaks/gitleaks/releases/tag/v8.16.3)] (!216)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!216)

## v5.0.0
- Bump to next major version (!214)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!214)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!214)

## v4.5.19
- Add word boundaries to Meta tokens (!215)

## v4.5.18
- Ensure enough commits for a commit-range scan (!212)

## v4.5.17
- Add Meta tokens (Meta, Oculus, Instagram) (!190)

## v4.5.16
- Add detection rule for Segment API tokens (!189)

## v4.5.15
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.15.2` => [`8.16.2`](https://github.com/gitleaks/gitleaks/releases/tag/v8.16.2)] (!213)
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!213)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!213)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!213)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!213)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!213)
- upgrade Go version to v1.19 for Non-FIPS container (!213)

## v4.5.14
- Add GCP OAuth client secret detection rule (!211)

## v4.5.13
- Add GitLab Feed token detection rule (!208)

## v4.5.12
- Match entire GCP Service Account private_key field (!209)

## v4.5.11
- Add DigitalOcean token detection rules (!178)

## v4.5.10
- Add Sendinblue STMP token pattern (!201)

## v4.5.9
- Add detection pattern for GCP API keys (!192)

## v4.5.8
- Add GitLab Runner Authentication Token detection rule (!198)

## v4.5.7
- Update Google Cloud service account key pattern to look for `private_key` value instead of `"type": "service_account"` (!191)

## v4.5.6
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.15.0` => [`8.15.2`](https://github.com/zricethezav/gitleaks/releases/tag/v8.15.2)] (!195)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!195)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!195)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!195)

## v4.5.5
- Correctly scan MR pipelines (!194)
- Simplify finding descriptions (!194)

## v4.5.4
- Add systemd 'machine-id' file detection rule (!182)

## v4.5.3
- Add `go.mod`/`go.sum` to global allowlist to prevent false positives found in hashes (!175)

## v4.5.2
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.14.1` => [`8.15.0`](https://github.com/zricethezav/gitleaks/releases/tag/v8.15.0)] (!187)
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!187)
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!187)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!187)

## v4.5.1
- Fix regexp of slack webhooks in gitleaks file (!186)

## v4.5.0
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.12.0` => [`8.14.1`](https://github.com/zricethezav/gitleaks/releases/tag/v8.14.1)] (!181)
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!181)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!181)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!181)

## v4.4.3
- Switch to use ubi8-minimal as the base FIPS image (!183)

## v4.4.2
- Bump go-fips builder image to 1.18 (!180)

## v4.4.1
- Update common to `v3.2.1` to fix gotestsum cmd (!177)

## v4.4.0
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.10.3` => [`8.12.0`](https://github.com/zricethezav/gitleaks/releases/tag/v8.12.0)] (!176)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!176)

## v4.3.2
- Add GitLab Pipeline Trigger Token detection rule (!174)

## v4.3.1
- Fix SECRET_DETECTION_HISTORIC_SCAN bug that caused an historic scan to be ran if env var was set regardless of value (!173)

## v4.3.0
- upgrade [`Gitleaks`](https://github.com/zricethezav/gitleaks) version [`8.8.11` => [`8.10.3`](https://github.com/zricethezav/gitleaks/releases/tag/v8.10.3)] (!172)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!172)
- upgrade `github.com/stretchr/testify` version [`v1.7.0` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!172)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!172)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!172)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!172)

## v4.2.3
- Preallocate vulnerabilities slice in `convert.go` (!168)

## v4.2.2
- Upgrade the `command` package for better analyzer messages (!170)

## v4.2.1
- Fix CA Certs for OpenShift environments (!169)

## v4.2.0
- Upgrade core analyzer dependencies (!165)
  - Adds support for globstar patterns when excluding paths
  - Adds analyzer details to the scan report

## v4.1.0
- Update `gitleaks.toml` to [include keywords](https://github.com/zricethezav/gitleaks#configuration) (!163)
- Update gitleaks to v8.8.11 (!163)
  - fix no-git bug (#859)
  - fix line number bug for secrets on last lines of a file (#853)
  - ignore end line when comparing generic rules (#879)
  - adding stopwords (#849)
  - fix out of bounds pre-filter (#843)
  - optimize keywords (#841)

## v4.0.6
- Removes SSN rules (!162)

## v4.0.5
- Expanding SSN rule allowlist (!161)

## v4.0.4
- When `git fetch` fails, use already checked out git environment (!160)

## v4.0.3
- Default to scan only one commit if `git log` range returns an err (!159)

## v4.0.2
- Log warning when git fetch/log errors are encountered (!158)

## v4.0.1
- Log debug information before returning (!157)

## v4.0.0
- Remove `SECRET_DETECTION_COMMIT*` options in favor of `SECRET_DETECTION_LOG_OPTIONS` (!156)
- Remove `SECRET_DETECTION_ENTROPY` (!156)
- Fix `SECRET_DETECTION_HISTORIC_SCAN` (!156)

## v3.27.1
- Add Yandex Cloud tokens detection rules (!142)

## v3.27.0
- Update gitleaks dependency to v8.6.1 (!154)
  - Normalize keyword check (#830)
  - Pre-regex-check keyword string compare (#825)
- Add custom CA suppport for FIPS docker image

## v3.26.1
- Add Dockerfile.fips for FIPS releases (!151)

## v3.26.0
- Update gitleaks dependency to v8.5.3 (!150)
  - skip content checks for path only rules
  - skip binary files with --no-git (#810)
  - Allow tag (#809)
  - Refactor detect, add entropy to all findings (#804)

## v3.25.5
- Tune password-in-url rule to reduce false positives (!138)

## v3.25.4
- Allow merge commits (!147)

## v3.25.3
- Fix invalid commit range error from not being caught (!146)

## v3.25.2
- Add missing character to GitLab PAT detection rule (!144)

## v3.25.1
- Add GitLab Runner Registration Token detection rule (!143)

## v3.25.0
- Update ruleset, report, and command modules to support ruleset overrides (!141)

## v3.24.8
- Disable Hachicorp Vault service token detection (!137)

## v3.24.7
- Check for no changes (!134)

## v3.24.6
- Pass `SECURE_LOG_LEVEL` value down to gitleaks' log level (!135)

## v3.24.5
- Restrict Hashicorp Vault service token detection regex (!136)

## v3.24.4
- Add Hashicorp Vault token detection rules (!133)

## v3.24.3
- Tune password-in-url rule to reduce false positives (!132)

## v3.24.2
- Log gitleaks output async (!130)
- Remove `GIT_DEPTH` dependency
- Update gitleaks dependency to v8.2.7 (!130)
  - remove godoc text filtering (#763)
  - limit number of goroutines for historic scanning (#761)
  - always write sarif results if report-path is set
  - limit goroutines on file scanning (#759)

## v3.24.1
- Update gitleaks dependency to v8.2.3 (!129)

## v3.24.0
- Update gitleaks dependency to v8.2.1 (!127)

## v3.23.1
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!128)

## v3.23.0
- Add: New secret detection rules (!126)

## v3.22.1
- chore: Update go to v1.17 (!125)

## v3.22.0
- Add [Age](https://age-encryption.org) secret key detection rule (!122)

## v3.21.0
- Add GitLab Personal Access Token regexes (!123)

## v3.20.1
- Resolve vulnerabilities (!120)

## v3.20.0
- Update gitleaks to `v7.5.0` (!112)

## v3.19.0
- Update gitleaks to `v7.4.0` (!109)
  - [`v7.4.0` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.4.0)
- Add the new [github token format](https://github.blog/changelog/2021-03-31-authentication-token-format-updates-are-generally-available/) rules

## v3.18.1
- Add `revocation_token` to PyPI rule (!107)

## v3.18.0
- Add PyPI token regexes (!104 @ewjoachim)

## v3.17.0
- Update report dependency in order to use the report schema version 14.0.0 (!53)

## v3.16.0
- Update gitleaks to `v7.3.0` (!101)
  - [`v7.3.0` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.3.0)

## v3.15.2
- Update json schema from 3.0.0 to 13.1.0 (!100)
- Add default SHA for `--no-git` scans (!100)

## v3.15.1
- Fix AWS token mismatch due to change in identifier (!98)

## v3.15.0
- Refactor the Secrets analyze to fit the Secure common interface. (!92)
- Update gitleaks to `v7.2.2` (!92)
  - [`v7.2.2` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.2.2)

## v3.14.0
- Update gitleaks to `v7.2.1` (!91)
  - [`v7.2.1` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.2.1)

## v3.13.2
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!90)

## v3.13.1
- Add Shopify token regexes (!89 @JackMc)
- Fix GitHub capitalization in gitleaks.toml (!88 @bbodenmiller)

## v3.13.0
- Upgrade common to v2.22.0 (!87)
- Update urfave/cli to v2.3.0 (!87)

## v3.12.0
- Add disablement of rulesets (!86)

## v3.11.1
- Add invalid line number warning message to the vulnerability description (!84)
- Change invalid line "-1" in vulnerability location to default to "1" (!84)

## v3.11.0
- Add social security number regex to gitleaks toml (!83)

## v3.10.2
- Bump gitleaks to v6.2.0 (!82)

## v3.10.1
- Add `vulnerability.raw_source_code_extract` containing leaked token (!79)

## v3.10.0
- Add custom rulesets (!80)

## v3.9.3
- Update golang dependencies (!75)

## v3.9.2
- Fix bug when `GIT_DEPTH` exceeds listed commits during non-default-branch scans (!72)

## v3.9.1
- Update Dockerfile and golang dependencies to latest versions (!71)

## v3.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!70)

## v3.8.0
- Bump gitleaks to v6.1.2 (!69)
- Add `SECRET_DETECTION_COMMITS` and `SECRET_DETECTION_COMMITS_FILE` options

## v3.7.2
- Upgrade go to version 1.15 (!68)

## v3.7.1
- Bump secrets module from v2 to v3 (!65)

## v3.7.0
- Replace `SAST_EXCLUDED_PATHS` with `SECRET_DETECTION_EXCLUDED_PATHS` (!64)

## v3.6.0
- Use scanner instead of analyzer in `scan.scanner` object (!62)

## v3.5.0
- Bump gitleaks to v5.0.1 (!60)

## v3.4.1
- Increase possible password length in URL regex (!61)

## v3.4.0
- Add scan object to report (!56)

## v3.3.1
- Fix `slack token` reporting (!57)

## v3.3.0
- Remove Trufflehog dependency from the analyzer (!52)

## v3.2.0
- Switch to the MIT Expat license (!54)

## v3.1.0
- Update logging to be standardized across analyzers (!50)

## v3.0.5
- Fixes `SECRET_DETECTION_HISTORIC_SCAN` bug when scanning non-existent files on the default branch (!48)

## v3.0.4
- Bump gitleaks to v4.3.1 (!49)

## v3.0.3
- Change env var prefix from `SAST_GITLEAKS_` to `SECRET_DETECTION_`

## v3.0.2
- Ignore QA test so we can release v3.x. v3.0.1 did not include a changelog entry so we need to bump once more.

## v3.0.1
- Ignore QA test so we can release v3.x

## v3.0.0
- Add standalone secret detection support (!42)

## v2.8.0
- Bump gitleaks and trufflehog (!38)

## v2.7.0
- Adds the `SAST_EXCLUDED_PATHS` env flag (!37)

## v2.6.0
- Add `id` field to vulnerabilities in JSON report (!36)

## v2.5.0
- Add commit range scanning

## v2.4.0
- Add historic scanning (!27)

## v2.3.0
- Add support for custom CA certs (!30)

## v2.2.3
- Update Gitleaks from 1.24.0 to 3.3.0

## v2.2.2
- Add check for env vars in password-in-url vulnerabilities

## v2.2.1
- Update common to v2.1.6

## v2.2.0
- Remove diffence (https://gitlab.com/gitlab-org/security-products/analyzers/secrets/merge_requests/13)

## v2.1.1
- Fix typos in reported messages

## v2.1.0
- Add support for generic api keys (https://gitlab.com/gitlab-org/gitlab/issues/10594)

## v2.0.5
- Set default severity value to Critical

## v2.0.4
- Fix: Set default Gitleaks entropy level to maximum to suppress false positives

## v2.0.3
- Fix: Update gitleaks file reader to better handle large files, fixes buffer overflow
- Fix: Update incorrect gitleaks rule RKCS8 to PKCS8

## v2.0.2
- Add gitleaks config, exclude svg analysis

## v2.0.1
- Fix gitleaks integration: don't parse output when there are no leaks

## v2.0.0
- Initial release

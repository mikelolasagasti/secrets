package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

const (
	flagHistoricScan          = "full-scan"
	flagGitLogOps             = "git-log-options"
	flagExcludedPaths         = "excluded-paths"
	defaultPathGitleaksConfig = "/gitleaks.toml"
	secretDetectionLogOpts    = "SECRET_DETECTION_LOG_OPTIONS"
	envVarFullScan            = "SECRET_DETECTION_HISTORIC_SCAN"
	noChangesMsg              = "no changes detected"
	maxSourceCodeLen          = 1750
	leaksExitCode             = "0"
	gitleaksPassthroughTarget = "gitleaks.toml"
	defaultCommitBeforeSHA    = "0000000000000000000000000000000000000000"
	// Retrieve all possible commits (see https://git-scm.com/docs/shallow).
	maxGitDepth = 2147483647
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagGitLogOps,
			Usage:   "Gitleaks git log options",
			EnvVars: []string{secretDetectionLogOpts},
		},
		&cli.BoolFlag{
			Name:    flagHistoricScan,
			Usage:   "Runs an historic (all commits) scan on the repository",
			EnvVars: []string{envVarFullScan},
		},
	}
}

// analyze runs the tools and produces a report containing issues for each detected secret leak.
func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(path, ruleset.PathSecretDetection)
	customRuleset, err := ruleset.Load(rulesetPath, "secrets", log.StandardLogger())
	if err != nil {
		switch err.(type) {
		case *ruleset.NotEnabledError:
			log.Debug(err)
		case *ruleset.ConfigFileNotFoundError:
			log.Debug(err)
		case *ruleset.ConfigNotFoundError:
			log.Debug(err)
		case *ruleset.InvalidConfig:
			log.Fatal(err)
		default:
			return nil, err
		}
	}

	// Create a temporary file. Gitleaks can't output to stdout.
	tmpFile, err := ioutil.TempFile("", "gitleaks-*.json")
	if err != nil {
		log.Errorf("Couldn't create temporary file: %v\n", err)
		return nil, err
	}

	pathGitleaksConfig, err := configPath(path, customRuleset)
	if err != nil {
		return nil, err
	}

	// default gitleaks log level is `info`. If SECURE_DEBUG_LEVEL is set we
	// should pass that level down to gitleaks as well
	logLevel := "info"
	if os.Getenv("SECURE_LOG_LEVEL") != "" {
		logLevel = os.Getenv("SECURE_LOG_LEVEL")
	}

	defaultOptions := []string{"detect", "--report-path", tmpFile.Name(),
		"--report-format", "json", "--source", path,
		"--config", pathGitleaksConfig, "--exit-code", leaksExitCode,
		"--log-level", logLevel}

	if err != nil {
		if err.Error() == noChangesMsg {
			return ioutil.NopCloser(strings.NewReader("[]")), nil
		}
		return nil, err
	}

	// fetch commits if needed
	if err := gitFetch(c); err != nil {
		return nil, err
	}

	options, err := buildOptions(c, defaultOptions)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command("gitleaks", options...)
	cmd.Env = os.Environ()
	if err := listenForOutput(cmd); err != nil {
		return nil, err
	}

	// start gitleaks command
	log.Debugf("Running gitleaks command: %s\n", cmd.String())
	if err := cmd.Start(); err != nil {
		return nil, err
	}

	// wait for gitleaks command git finish
	if err := cmd.Wait(); err != nil {
		return nil, err
	}

	return os.Open(tmpFile.Name())
}

func buildOptions(c *cli.Context, options []string) ([]string, error) {
	if c.IsSet(flagGitLogOps) {
		return append(options, "--log-opts", c.String(flagGitLogOps)), nil
	} else if c.Bool(flagHistoricScan) {
		return options, nil
	}
	// default options will run a --no-git scan
	return append(options, "--no-git"), nil
}

// configPath will look at rulesets to determine the path for the gitleaks.toml
func configPath(projectPath string, customRuleset *ruleset.Config) (string, error) {
	// Set path to default
	pathGitleaksConfig := defaultPathGitleaksConfig

	if customRuleset == nil {
		return pathGitleaksConfig, nil
	}

	for _, passthrough := range customRuleset.Passthrough {
		if passthrough.Target == gitleaksPassthroughTarget {
			switch passthrough.Type {
			case ruleset.PassthroughFile:
				pathGitleaksConfig = filepath.Join(projectPath, passthrough.Value)
			case ruleset.PassthroughRaw:
				content := []byte(passthrough.Value)
				tmpfile, err := ioutil.TempFile("", "gitleaks*.toml")
				if err != nil {
					return "", err
				}

				log.Debugf("Gitleaks config: %s", passthrough.Value)
				if _, err := tmpfile.Write(content); err != nil {
					return "", err
				}

				if err := tmpfile.Close(); err != nil {
					return "", err
				}

				pathGitleaksConfig = tmpfile.Name()
				log.Debugf("Gitleaks config path: %s", pathGitleaksConfig)
			}
		}
	}

	return pathGitleaksConfig, nil
}

func listenForOutput(cmd *exec.Cmd) error {
	// Listen for stderr coming from gitleaks
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	scannerStdErr := bufio.NewScanner(stderr)
	go func() {
		for scannerStdErr.Scan() {
			m := scannerStdErr.Text()
			log.Info(m)
		}
	}()

	// Listen for stdout coming from gitleaks
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	scannerStdOut := bufio.NewScanner(stdout)
	go func() {
		for scannerStdOut.Scan() {
			m := scannerStdOut.Text()
			log.Info(m)
		}
	}()
	return nil
}

func gitFetch(c *cli.Context) error {
	commitBeforeSHA := os.Getenv("CI_COMMIT_BEFORE_SHA")
	commitSHA := os.Getenv("CI_COMMIT_SHA")
	defaultBranch := os.Getenv("CI_DEFAULT_BRANCH")
	commitBranch := os.Getenv("CI_COMMIT_BRANCH")
	commitRefName := os.Getenv("CI_COMMIT_REF_NAME")
	mrDiffBaseSHA := os.Getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA")

	runAndLog := func(cmd *exec.Cmd) error {
		output, err := cmd.CombinedOutput()
		if err != nil {
			log.Warnf("Error encountered when running git command: %s\noutput: %s\nerror:%v",
				cmd.String(),
				string(output),
				err)
			return err
		}
		return nil
	}
	fetchAllCommits := func(ref string) error {
		return runAndLog(exec.Command("git", "fetch", fmt.Sprintf("--depth=%d", maxGitDepth), "origin", ref))
	}
	if c.Bool(flagHistoricScan) {
		// need to get fetch --all for a historic scan
		if err := runAndLog(exec.Command("git", "fetch", "--all")); err != nil {
			log.Warn("Continuing with already checked out git environment")
		}
		return nil

	} else if defaultBranch == commitBranch {
		// do nothing, if the branch is the same as the default branch we don't
		// need to git fetch since we are scanning using `--no-git`
		return nil
	} else if c.IsSet(flagGitLogOps) {
		// if --log-opts is set already we cannot be sure of the depth of the scan
		// therefore we need to fetch the entire history of the branch
		if err := fetchAllCommits(commitRefName); err != nil {
			log.Warn("Continuing with already checked out git environment")
		}
		return nil
	} else if mrDiffBaseSHA != "" {
		// we're running on an MR branch and need to use the correct commit range
		commitBeforeSHA = mrDiffBaseSHA
	} else if commitBeforeSHA == defaultCommitBeforeSHA {
		// if this condition is met this is the first commit on a new branch
		// by default we don't need to fetch anything since `GIT_DEPTH` is set
		// to 50.
		// Set logOptions for gitleaks

		cmd := exec.Command("git", "rev-list", "HEAD", "--count")
		commitCount, err := cmd.CombinedOutput()
		if err != nil {
			return fmt.Errorf("get commit count: %w", err)
		}
		// We need to ensure that enough commits exist on this branch to perform
		// a commit-range scan. The ^ gets the parent, which is troublesome if there
		// is _no_ parent.
		// Note: count is inclusive of `commitSHA`, which is why we're not comparing
		// against "0".
		if string(commitCount) != "1" {
			c.Set(flagGitLogOps, fmt.Sprintf("%s^..%s", commitSHA, commitSHA))
		}

		return nil
	}

	// if this a Merge Request, fetch all possible commits for the branch
	err := fetchAllCommits(commitRefName)
	if err != nil {
		log.Warnf("Defaulting to scan only %s", commitSHA)
		c.Set(flagGitLogOps, fmt.Sprintf("%s^..%s", commitSHA, commitSHA))
		return nil
	}

	// make sure that the diff base exists
	// if not, the git history may have been rewritten
	if err := runAndLog(exec.Command("git", "cat-file", "-t", commitBeforeSHA)); err != nil {
		log.Warn("Failed to retrieve all the commits from the last Git push event due to a force push. Continuing with already checked out git environment")
		return nil
	}

	// Set logOptions for gitleaks.
	c.Set(flagGitLogOps, fmt.Sprintf("%s..%s", commitBeforeSHA, commitSHA))

	return nil
}
